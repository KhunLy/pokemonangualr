import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PokemonsProvider {

  private endPoint: string = 'https://pokeapi.co/api/v2/pokemon';
  constructor(
    private httpClient: HttpClient
  ) { }

  getAll() : Observable<any> {
    return this.httpClient.get(this.endPoint);
  }

  getDetails(url: string) : Observable<any>{
    return this.httpClient.get(url);
  }
}
