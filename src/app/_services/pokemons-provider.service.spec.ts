import { TestBed } from '@angular/core/testing';

import { PokemonsProviderService } from './pokemons-provider.service';

describe('PokemonsProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PokemonsProviderService = TestBed.get(PokemonsProviderService);
    expect(service).toBeTruthy();
  });
});
