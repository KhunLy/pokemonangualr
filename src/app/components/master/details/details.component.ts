import { Component, OnInit, Input } from '@angular/core';
import { Pokemon } from 'src/app/_models/pokemon';
import { PokemonsProvider } from 'src/app/_services/pokemons-provider.service';
import { PokemonDetails } from 'src/app/_models/pokemonDetails';

@Component({
  selector: 'master-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  public pokemonDetails: PokemonDetails;

  private _pokemon: Pokemon;

  @Input('pokemon')
  set pokemon(value) {
    this._pokemon = value;
    if(value != null){
      this.pokeProv.getDetails(this._pokemon.url)
      .subscribe(data => {
        this.pokemonDetails = data;
      });
    }
  }

  get pokemon(): Pokemon {
    return this._pokemon;
  };
  

  constructor(
    private pokeProv: PokemonsProvider
  ) { }

  ngOnInit() {
  }

}
