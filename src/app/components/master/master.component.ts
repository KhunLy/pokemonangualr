import { Component, OnInit } from '@angular/core';
import { HttpClient } from 'selenium-webdriver/http';
import { PokemonsProvider } from 'src/app/_services/pokemons-provider.service';
import { Pokemon } from 'src/app/_models/pokemon';

@Component({
  selector: 'master',
  templateUrl: './master.component.html',
  styleUrls: ['./master.component.scss']
})
export class MasterComponent implements OnInit {

  private list: Array<Pokemon> = []

  private selectedItem: Pokemon;

  constructor(
    private pokeProv: PokemonsProvider
  ) { }

  ngOnInit() {
    this.pokeProv.getAll()
      .subscribe((data) => {
        this.list = data['results'];
      })
  }

  selectItem(item: Pokemon){
    this.selectedItem = item;
  }

}
